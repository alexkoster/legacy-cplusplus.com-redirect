<!--

SPDX-License-Identifier: BSD-3-Clause

The New BSD License

Copyright (c) 2014, Tom Watson
Copyright (c) 2022, Alexander Koster, United States of America
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

# Legacy cplusplus.com Redirect

[Mozilla Firefox Extension](https://addons.mozilla.org/en-US/firefox/addon/legacy-cplusplus-com-redirect/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search)

Dislike cplusplus.com's redesign? Legacy cplusplus.com Redirect will ensure 
that you always load the old (legacy) cplusplus.com design instead.

This addon will force all cplusplus.com usage to legacy.cplusplus.com.  This
addon will work when navigating to the site, opening links, or when using old 
bookmarks.  Additionally, this addon works regardless of whether you are 
logged into the forum or not or are using a private window.

#### Redirected domains

- `cplusplus.com`
- `www.cplusplus.com`

#### Whitelisted domains

- none

## License

Copyright (c) 2022, Alexander Koster, United States of America
Portions copyright (c) 2014, Tom Watson

Code released under [The New BSD License](LICENSE.txt).

Forked from [https://github.com/tom-james-watson/old-reddit-redirect.git](https://github.com/tom-james-watson/old-reddit-redirect).

Icon modified from [original SVG file](https://commons.wikimedia.org/wiki/File:C_plus_plus.svg) on [Wikimedia Commons](https://commons.wikimedia.org/) created by [JTojnar](https://commons.wikimedia.org/wiki/User:JTojnar) in [2009](https://upload.wikimedia.org/wikipedia/commons/archive/5/5b/20160417210141%21C_plus_plus.svg) and modified by [Oliver H](https://commons.wikimedia.org/wiki/User:Oliver_H) in [2016](https://upload.wikimedia.org/wikipedia/commons/5/5b/C_plus_plus.svg).
